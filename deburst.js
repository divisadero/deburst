/**
 * Extracts the param that makes the burst call and makes calls with a timeout, accumulating the burst in an array
 * It holds the rest of params as they are.
 *
 * fn should be a function with the first argument causing the burst, and the rest used to calculate if it should be
 * cached debursted or not.
 * @param {Function} fn - To be called on timeout with the accumulated burst
 * @param {Number} time - millis to wait until the call
 * @return {Function} Debursted function
 */
function deburst(fn, time) {
  const cache = {}; // holds the timeouts
  return function (...args) {
    const burster = args.shift();
    const key = args.join('-') || '-';
    const {timeout: oldTimeout} = cache[key] || {};
    // if there's a timeout running for this key stop it and store the burster
    if (oldTimeout) {
      clearTimeout(oldTimeout);
      cache[key].bursters.push(burster);
    }
    // otherwise this is the first call, create a new cache element
    else {
      cache[key] = {bursters: [].concat(burster)};
    }
    const newTimeout = setTimeout(async () => {
      const {bursters} = cache[key];
      delete cache[key];
      fn.apply(null, [bursters, ...args]);
    }, time);
    cache[key].timeout = newTimeout;
  }
}

module.exports = {deburst};
