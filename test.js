const {deburst} = require('../deburst');

jest.useFakeTimers();

const WAIT_TIME = 1000;
const mockFunction = jest.fn();
const deburstedFn = deburst(mockFunction, WAIT_TIME);

beforeEach(() => {
  jest.clearAllMocks();
  jest.clearAllTimers();
});

test('applies params correctly', () => {
  deburstedFn('file1.png', 'tabla-A', 'document-B', 'database-C', 'param-D');
  jest.runAllTimers();
  expect(mockFunction).toHaveBeenCalledWith(['file1.png'], 'tabla-A', 'document-B', 'database-C', 'param-D');
});

test('joins consecutive calls', () => {
  deburstedFn('file1.png', 'tabla-A');
  jest.advanceTimersByTime(WAIT_TIME / 2);
  deburstedFn('file2.png', 'tabla-A');
  jest.runAllTimers();
  expect(mockFunction).toHaveBeenCalledWith(['file1.png', 'file2.png'], 'tabla-A');
});

test('split distant calls', () => {
  deburstedFn('file1.png', 'tabla-A');
  jest.advanceTimersByTime(WAIT_TIME * 2);
  deburstedFn('file2.png', 'tabla-A');
  jest.runAllTimers();
  expect(mockFunction).toHaveBeenNthCalledWith(1, ['file1.png'], 'tabla-A');
  expect(mockFunction).toHaveBeenNthCalledWith(2, ['file2.png'], 'tabla-A');
});

test('deburst works', () => {
  deburstedFn('file1.png', 'tabla-A');
  deburstedFn('file2.png', 'tabla-B');
  deburstedFn('file3.png', 'tabla-A');
  deburstedFn('file4.png', 'tabla-B');
  jest.advanceTimersByTime(WAIT_TIME / 2);
  deburstedFn('file5.png', 'tabla-A');
  deburstedFn('file6.png', 'tabla-B');
  jest.advanceTimersByTime(WAIT_TIME * 1.1);
  deburstedFn('file7.png', 'tabla-A');
  deburstedFn('file8.png', 'tabla-B');
  jest.runAllTimers();

  expect(mockFunction).toHaveBeenNthCalledWith(1, ['file1.png', 'file3.png', 'file5.png'], 'tabla-A');
  expect(mockFunction).toHaveBeenNthCalledWith(2, ['file2.png', 'file4.png', 'file6.png'], 'tabla-B');
  expect(mockFunction).toHaveBeenNthCalledWith(3, ['file7.png'], 'tabla-A');
  expect(mockFunction).toHaveBeenNthCalledWith(4, ['file8.png'], 'tabla-B');
});
